module.exports = {
  collectCoverage: true,
  moduleFileExtensions: ['js'],
  transform: {},
  moduleNameMapper: {},
  snapshotSerializers: [],
  testMatch: [
    '**/test/*.spec.js'
  ],
  testURL: 'http://localhost/'
}
